import 'package:riverpod/riverpod.dart';
import 'package:test_di/account_dependencies/account_holder.dart';
import 'package:test_di/account_dependencies/item_list_provider.dart';
import 'package:test_di/app_scope.dart';

import 'account_dependencies/internal_sdk_provider.dart';
import 'di.dart';

class AccountScope extends DI {
  final AppScope appScope;
  static AccountScope? _instance;

  static AccountScope get instance =>
      _instance ??= AccountScope._(AppScope.instance);

  AccountScope._(this.appScope) {
    initFutureProviders([internalSdk]);
  }

  final accountHolder = Provider<AccountHolder>((ref) => AccountHolder(null));

  late final internalSdk =
      futureToProvider((ref) async => InternalSdkProvider.create(
            await ref.read(appScope.platform),
            await ref.read(accountHolder),
          ));

  late final itemList = Provider((ref) => ItemListProvider(
        ref.watch(accountHolder),
        ref.watch(internalSdk),
      ));
}
