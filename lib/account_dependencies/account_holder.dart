import 'package:riverpod/riverpod.dart';

class AccountHolder extends StateNotifier<String?> {
  AccountHolder(String? account) : super(account);

  String get account => state!;
}
