import 'package:test_di/app_depenencies/platform_provider.dart';

class AppInfoProvider {
  final PlatformProvider _platformProvider;
  final String _loggerTag;

  AppInfoProvider(
    this._platformProvider,
    this._loggerTag,
  );

  Map<String, dynamic> get info => {
        'platform': _platformProvider.platform,
        'loggerTag': _loggerTag,
      };
}
