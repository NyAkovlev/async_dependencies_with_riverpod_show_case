import 'package:riverpod/riverpod.dart';
import 'package:test_di/app_depenencies/environment_provider.dart';
import 'package:test_di/app_depenencies/login_provider.dart';
import 'package:test_di/app_depenencies/platform_provider.dart';

import 'app_depenencies/app_info_provider.dart';
import 'di.dart';

class AppScope extends DI {
  static AppScope? _instance;

  static AppScope get instance => _instance ??= AppScope._();

  AppScope._() {
    initFutureProviders([login, platform]);
  }

  late final loggerTag = Provider<String>((ref) => throw UnimplementedError());

  late final environment =
      Provider<EnvironmentProvider>((ref) => EnvironmentProvider());

  late final platform =
      futureToProvider<PlatformProvider>((ref) async => PlatformProvider.create(
            await ref.read(environment),
          ));

  late final appInfo = Provider((ref) => AppInfoProvider(
        ref.watch(platform),
        ref.read(loggerTag),
      ));

  late final login = futureToProvider((ref) async => LoginProvider.create(
        await ref.read(platform),
        await ref.read(loggerTag),
      ));
}
