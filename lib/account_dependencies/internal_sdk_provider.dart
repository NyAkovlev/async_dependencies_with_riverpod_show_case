import 'package:test_di/account_dependencies/account_holder.dart';
import 'package:test_di/app_depenencies/platform_provider.dart';

class InternalSdkProvider {
  InternalSdkProvider._();

  static Future<InternalSdkProvider> create(
    PlatformProvider _platformProvider,
    AccountHolder _accountHolder,
  ) async {
    await Future.delayed(Duration(seconds: 1));
    return InternalSdkProvider._();
  }
}
