import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod/riverpod.dart';
import 'package:test_di/account_scope.dart';
import 'package:test_di/app_scope.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
          child: ProviderScope(
            overrides: [
              AppScope.instance.loggerTag.overrideWithValue('first'),
            ],
            child: MaterialApp(
              home: ScopeLoadingWidget(
                futureProvider: AppScope.instance.initProvider,
                child: AppRootWidget(),
              ),
            ),
          ),
        ),
        Flexible(
          child: ProviderScope(
            overrides: [
              AppScope.instance.loggerTag.overrideWithValue('second'),
            ],
            child: MaterialApp(
              home: ScopeLoadingWidget(
                futureProvider: AppScope.instance.initProvider,
                child: AppRootWidget(),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class ScopeLoadingWidget extends ConsumerWidget {
  final Widget child;
  final FutureProvider<bool> futureProvider;

  ScopeLoadingWidget({
    required this.futureProvider,
    required this.child,
  });

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return watch(futureProvider).when(data: (isCompleted) {
      if (isCompleted) {
        return child;
      } else {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
    }, loading: () {
      return Center(
        child: CircularProgressIndicator(),
      );
    }, error: (e, s) {
      return ErrorWidget(e);
    });
  }
}

class AppRootWidget extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final info = watch(AppScope.instance.appInfo);
    final login = watch(AppScope.instance.login);
    final accountHolder = watch(AccountScope.instance.accountHolder);
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(info.info.toString()),
            TextButton(
              child: Text('init account'),
              onPressed: () async {
                final account = await login.login();
                accountHolder.state = account;
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => AccountRootWidget(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class AccountRootWidget extends ConsumerWidget {
  AccountRootWidget();

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return ScopeLoadingWidget(
      futureProvider: AccountScope.instance.initProvider,
      child: Consumer(
        builder: (context, watch, _) {
          final itemList = watch(AccountScope.instance.itemList).items;
          return Scaffold(
              appBar: AppBar(),
              body: ListView.builder(
                  itemCount: itemList.length,
                  itemBuilder: (BuildContext context, int index) =>
                      ListTile(title: Text(itemList[index]))));
        },
      ),
    );
  }
}
