import 'package:test_di/account_dependencies/account_holder.dart';
import 'package:test_di/account_dependencies/internal_sdk_provider.dart';

class ItemListProvider {
  final AccountHolder _accountHolder;
  final InternalSdkProvider _internalSdk;

  ItemListProvider(
    this._accountHolder,
    this._internalSdk,
  );

  List<String> get items {
    return List.generate(10, (index) => '${_accountHolder.account}`s item 1');
  }
}
