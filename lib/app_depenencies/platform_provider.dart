import 'dart:io';

import 'environment_provider.dart';

class PlatformProvider {
  final EnvironmentProvider _environmentProvider;

  PlatformProvider._(this._environmentProvider);

  String get platform => _environmentProvider.isTest
      ? "testing ${Platform.operatingSystem}"
      : Platform.operatingSystem;

  static Future<PlatformProvider> create(
    EnvironmentProvider environmentProvider,
  ) async {
    await Future.delayed(Duration(seconds: 1));
    return PlatformProvider._(environmentProvider);
  }
}
