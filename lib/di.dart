import 'dart:async';

import 'package:riverpod/riverpod.dart';

class DI {
  late final List<_Provider> _futureProviders;

  void initFutureProviders(List<_Provider> futureProviders) =>
      _futureProviders = futureProviders;

  late final FutureProvider<bool> initProvider = FutureProvider((ref) async {
    //нужно прослушать все providers, что бы при последующем вызове callback не было изменений в логике
    final results = _futureProviders
        .map((provider) => ref.watch(provider.futureProvider.state))
        .toList();
    for (final result in results) {
      final completed = result.when(
        data: (_) => true,
        loading: () => false,
        error: (e, s) => throw _Error(e, s),
      );
      if (!completed) {
        return false;
      }
    }
    return true;
  });

  _Provider<T> futureToProvider<T>(
    Future<T> Function(_ProviderReference) create,
  ) {
    final futureProvider = StateNotifierProvider(
        (ref) => _Future(create(_ProviderReference(ref))));
    final provider = _Provider<T>(futureProvider,
        (ref) => ref.read(futureProvider).asyncData.data!.value);

    return provider;
  }
}

class _Error {
  final Object error;
  final StackTrace? stackTrace;

  _Error(this.error, this.stackTrace);
}

class _Future<T> extends StateNotifier<AsyncValue<T>> {
  final Future<T> _future;

  AsyncValue get asyncData => state;

  _Future(this._future) : super(AsyncValue.loading()) {
    _future.then((value) {
      state = AsyncValue.data(value);
    }).catchError((e, st) {
      state = AsyncValue.error(e, st);
    });
  }
}

class _Provider<T> extends Provider<T> {
  final StateNotifierProvider<_Future<T>> futureProvider;

  _Provider(this.futureProvider, Create<T, ProviderReference> create)
      : super(create);
}

class _ProviderReference {
  final ProviderReference _base;

  _ProviderReference(this._base);

  ProviderContainer get container => _base.container;

  bool get mounted => _base.mounted;

  void onDispose(void Function() cb) => _base.onDispose(cb);

  FutureOr<T> read<T>(RootProvider<Object?, T> provider) {
    if (provider is _Provider<T>) {
      final futureProvider = provider.futureProvider;
      final value = _base.read(futureProvider);
      return value._future;
    } else {
      return _base.read(provider);
    }
  }
}
