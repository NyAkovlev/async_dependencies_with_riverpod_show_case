import 'package:test_di/app_depenencies/platform_provider.dart';

class LoginProvider {
  final PlatformProvider platform;
  final String _loggerTag;

  LoginProvider._(this.platform, this._loggerTag);

  static Future<LoginProvider> create(
      PlatformProvider environmentProvider, String loggerTag) async {
    await Future.delayed(Duration(seconds: 1));
    return LoginProvider._(
      environmentProvider,
      loggerTag,
    );
  }

  Future<String> login() async {
    Future.delayed(Duration(seconds: 1));
    return "user($_loggerTag)";
  }
}
